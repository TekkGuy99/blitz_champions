// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerField.h"
#include "MainMenu.h"
#include "Components/Button.h"

void UServerField::setup(class UMainMenu* inParent, uint32 inIndex)
{
	parent = inParent;
	index = inIndex;

	SelectServerButton->OnClicked.AddDynamic(this, &UServerField::OnClicked);
}

void UServerField::OnClicked()
{
	parent->selectIndex(index);
}